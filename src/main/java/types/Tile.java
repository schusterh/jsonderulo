package types;

public class Tile {

    public int height;
    public int tileIndex;

    public Tile(int height, int tileIndex) {
        this.height = height;
        this.tileIndex = tileIndex;
    }
}
