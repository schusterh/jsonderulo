package types;

public enum GameMode {
    NORMAL,
    TERRAIN,
    BUILDING,
    DEMOLITION
}