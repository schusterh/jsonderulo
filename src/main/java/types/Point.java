package types;

public class Point {
    public Double x;
    Double y;
    public Point(Double x, Double y){
        this.x = x;
        this.y = y;
    }

    public Double getX() {
        return x;
    }

    public Double getY() {
        return y;
    }
}